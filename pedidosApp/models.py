from django.db import models

# Modelo del pedido de prestamo
class Pedidos(models.Model):

    #ID para poder generar mas de un pedido para un mismo DNI
    id = models.IntegerField(auto_created=True, primary_key=True)
    dni = models.IntegerField()
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    genero = models.CharField(max_length=10 )
    email = models.EmailField()
    monto = models.FloatField()
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.nombre