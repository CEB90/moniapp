"""moniApi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path
from django.views.generic.base import TemplateView
from pedidosApp import views



urlpatterns = [
    path('admin/', admin.site.urls),
    path('inicio/', views.inicio, name='inicio'),    
    path('crud/', login_required(views.PedidosListado.as_view(template_name = "crud.html")), name='crud'),
    path('crud/actualizar/<int:pk>', views.PedidosActualizar.as_view(template_name = "actualizar.html"), name='actualizar'),
    path('crud/eliminar/<int:pk>', views.PedidosEliminar.as_view(), name='eliminar'), 
    path('accounts/', include('django.contrib.auth.urls')),       
]
